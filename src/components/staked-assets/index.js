import React, {useEffect, useMemo, useState} from 'react';
import {Button, Table} from "antd";
import {displayLogo, displayPeriod, displayTokenRewardInterest, LinkToAddressToken, toEther} from "../../utils";
import {UseWeb3AssetContext} from "../../App";
import './index.css'
import moment from 'moment'
import {ethers} from "ethers";

const StakedAssets = props => {
    const {
        contract,
        signer,
        tokens,
        setReloadStakeAssets,
        tokenRewardInfo
    } = UseWeb3AssetContext()
    const [totalCount, setTotalCount] = useState()
    const [dataSource, setDataSource] = useState([])
    const [dateState, setDateState] = useState(new Date());
    useEffect(() => {
        setInterval(() => setDateState(new Date()), 3000);
    }, []);


    useMemo(async () => {
        const onLoad = async () => {
            await setDataSource([])
            const depositIdsHex = await contract.connect(signer).getDepositIdsByWalletAddress()
            const depositIds = depositIdsHex.map(id => Number(id))


            const deposits = await Promise.all(
                depositIds.map(id =>
                    contract.connect(signer).getDepositByDepositId(
                        Number(id)
                    ))
            )


            setTotalCount(deposits.length)


            deposits.map(async deposit => {
                const token = tokens[deposit.tokenAddress]


                const calculateDepositInterest = await contract.connect(signer).calculateAnticipatedInterest(deposit.depositId)

                const data = {
                    ...deposit,
                    asset: token.asset,
                    symbol: token.symbol,
                    ethAccruedInterest: Number(ethers.utils.formatEther(String(calculateDepositInterest))).toFixed(4)
                }
                setDataSource(prev => [...prev, data])
            })
        }
        await onLoad()

    }, [])


    const columns = [
        {
            title: 'Token Address',
            dataIndex: 'tokenAddress',
            key: 'address',
            render: (text) => {
                return LinkToAddressToken(text)
            }
        },
        {
            title: "Asset",
            dataIndex: "asset",
            key: 'asset',
            render: (text, record) => {
                return displayLogo(record.symbol)
            }
        },
        {
            title: "Tokens Staked",
            dataIndex: "tokenQuantity",
            key: "tokenQuantity",
            render: (text) => {
                return toEther(text)
            }
        },
        {
            title: "Accrued Interest",
            dataIndex: "ethAccruedInterest",
            key: "ethAccruedInterest",
            render: (text, record) => {
                if (record?.isUnlimited)
                    return displayTokenRewardInterest(text, tokenRewardInfo, null, true)
                else
                    return displayTokenRewardInterest(text, tokenRewardInfo)
            }
        },
        {
            title: "Interest",
            dataIndex: "interest",
            key: "interest",
            render: (text, record) => {
                const ethValue = toEther(record?.ethValue)
                const interestRate = Number(record?.interestRate)
                let numberDays = Number(record?.numberDays)
                if (numberDays === 0)
                    numberDays = 365

                const timeInSeconds = parseInt(record?.createdDate?._hex, 16)


                const interest =
                    (ethValue * ((moment() / 1000) - moment(timeInSeconds)) * interestRate) / (1000 * numberDays * 86400)
                if (!record?.open)
                    return "Close"
                if (interest > 0) {
                    if (interest > record?.ethAccruedInterest && !record?.isUnlimited) {
                        return record?.ethAccruedInterest
                    } else
                        return displayTokenRewardInterest(interest, tokenRewardInfo, null, false, 18)
                } else return 0


            }
        },
        {
            title: "Interest Rate",
            dataIndex: "interestRate",
            key: "interestRate",
            render: (text, record) => {
                if (record?.isUnlimited)
                    return Number(record?.interestRate) / 10 + "% / year"
                return Number(record?.interestRate) / 10 + "%"
            }
        },
        {
            title: "Period",
            dataIndex: "numberDays",
            key: "numberDays",
            render: (text) => {
                return displayPeriod(text)
            }
        },
        {
            title: "Created Date",
            dataIndex: "createdDate",
            key: "createdDate",
            render: (text) => {
                const timeInSeconds = parseInt(text._hex, 16)
                return moment(timeInSeconds * 1000).format("DD/MM/YYYY HH:mm:ss")
            }
        }, {
            title: "Excepted Closing Date",
            dataIndex: "createdDate",
            key: "expectedClosingDate",
            render: (text, record) => {
                const timeInSeconds = parseInt(text._hex, 16)
                return moment(timeInSeconds * 1000).add(Number(record?.numberDays), 'days').format("DD/MM/YYYY HH:mm:ss")
            }
        },
        {
            title: "Closing Day",
            dataIndex: "closingDate",
            key: "closingDate",
            render: (text) => {
                const timeInSeconds = parseInt(text._hex, 16)
                if (timeInSeconds !== 0)
                    return moment(timeInSeconds * 1000).format("DD/MM/YYYY HH:mm:ss")
            }
        },
        {
            title: "",
            dataIndex: "open",
            key: "open",
            render: (text, record) => {
                return <>
                    {text ? <Button type={"primary"}
                                    onClick={async () => {
                                        const res = await contract.connect(signer).closeDeposit(record.depositId)
                                        await res.wait()
                                        setReloadStakeAssets(true)
                                    }}
                        >Withdraw</Button>
                        : <Button disabled={true}>Close</Button>}
                </>

            }
        }
    ]


    const showTotal = (total) => `Total ${total} items`;
    return (
        <Table
            columns={columns}
            key={dateState.getTime()}
            dataSource={dataSource}
            pagination={{
                total: totalCount,
                showTotal: showTotal,
                pageSize: 1000
            }}
        />
    )
        ;
};

StakedAssets.propTypes = {};

export default StakedAssets;